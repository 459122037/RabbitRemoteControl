<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CFrmFullScreenToolBar</name>
    <message>
        <location filename="../../FrmFullScreenToolBar.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="31"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="35"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="36"/>
        <source>Nail</source>
        <translation>钉住</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="39"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="40"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="41"/>
        <source>Full</source>
        <translation>关闭全屏</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="53"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="54"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="55"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="48"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="49"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="50"/>
        <source>Disconnect</source>
        <translation>关闭边连接</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="42"/>
        <source>TabBar</source>
        <translation>标签条</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="45"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="46"/>
        <source>Tab bar</source>
        <translation>标签条</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.ui" line="14"/>
        <source>Rabbit Remote Control</source>
        <translation>玉兔远程控制</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="32"/>
        <source>Help(&amp;H)</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="39"/>
        <source>View(&amp;V)</source>
        <translation>视图(&amp;V)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="71"/>
        <source>Remote(&amp;R)</source>
        <translation>远程(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="96"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="124"/>
        <source>About(&amp;A)</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="127"/>
        <location filename="../../mainwindow.ui" line="130"/>
        <location filename="../../mainwindow.ui" line="133"/>
        <source>About(A)</source>
        <translation>关于(A)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="138"/>
        <source>Update(&amp;U)</source>
        <translation>更新(&amp;U)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="141"/>
        <location filename="../../mainwindow.ui" line="144"/>
        <source>Update(U)</source>
        <translation>更新(U)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="155"/>
        <source>StatusBar(&amp;S)</source>
        <translation>状态条(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="158"/>
        <location filename="../../mainwindow.ui" line="161"/>
        <source>StatusBar(S</source>
        <translation>状态条</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="172"/>
        <source>ToolBar(&amp;T)</source>
        <translation>工具条(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="175"/>
        <location filename="../../mainwindow.ui" line="178"/>
        <source>ToolBar(T)</source>
        <translation>工具条(T)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="187"/>
        <location filename="../../mainwindow.cpp" line="169"/>
        <source>Full screen(&amp;F)</source>
        <translation>全屏(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="232"/>
        <source>Zoom to window(&amp;Z)</source>
        <translation>缩放到窗口大小(&amp;Z)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="235"/>
        <source>Zoom to window</source>
        <translation>缩放到窗口大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="238"/>
        <location filename="../../mainwindow.ui" line="241"/>
        <location filename="../../mainwindow.ui" line="244"/>
        <source>Zoom to windows</source>
        <translation>缩放到窗口大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="256"/>
        <source>Keep aspect ration to window(&amp;K)</source>
        <translation>保持纵横比缩放到窗口大小(&amp;K)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="259"/>
        <source>Keep aspect ration to window</source>
        <translation>保持纵横比缩放到窗口大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="360"/>
        <location filename="../../mainwindow.ui" line="363"/>
        <location filename="../../mainwindow.ui" line="366"/>
        <source>Show TabBar</source>
        <translation>显示标签条</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="378"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="390"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="214"/>
        <source>Original size(&amp;O)</source>
        <translation>原始大小(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="217"/>
        <location filename="../../mainwindow.ui" line="220"/>
        <source>Original(O)</source>
        <translation>原始大小(O)</translation>
    </message>
    <message>
        <source>Zoom(Z)</source>
        <translation type="vanished">缩放(Z)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="262"/>
        <location filename="../../mainwindow.ui" line="265"/>
        <source>Keep AspectRation(K)</source>
        <translation>保持纵横比缩放</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="277"/>
        <source>Exit(&amp;E)</source>
        <translation>退出(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="280"/>
        <location filename="../../mainwindow.ui" line="283"/>
        <source>Exit(E)</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="339"/>
        <source>Send Ctl+Alt+Del</source>
        <translation>发送 Ctl+Alt+Del</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="354"/>
        <location filename="../../mainwindow.ui" line="357"/>
        <source>Show TabBar(&amp;B)</source>
        <translation>显示标签条(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="75"/>
        <source>Connect(&amp;C)</source>
        <translation>连接(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="288"/>
        <location filename="../../mainwindow.ui" line="291"/>
        <location filename="../../mainwindow.ui" line="294"/>
        <source>Recently connected</source>
        <translation>最近连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="303"/>
        <source>Disconnect(&amp;D)</source>
        <translation>断开连接(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="306"/>
        <location filename="../../mainwindow.ui" line="309"/>
        <source>Disconnect(D)</source>
        <translation>关闭连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="318"/>
        <location filename="../../mainwindow.ui" line="334"/>
        <source>Open(&amp;O)</source>
        <translation>打开(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="321"/>
        <location filename="../../mainwindow.ui" line="324"/>
        <source>Open(O)</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="329"/>
        <source>Default(&amp;D)</source>
        <translation>默认(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="43"/>
        <source>Sink</source>
        <translation>换肤</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="69"/>
        <location filename="../../mainwindow.cpp" line="70"/>
        <location filename="../../mainwindow.cpp" line="71"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="190"/>
        <location filename="../../mainwindow.ui" line="193"/>
        <location filename="../../mainwindow.ui" line="196"/>
        <location filename="../../mainwindow.ui" line="199"/>
        <location filename="../../mainwindow.cpp" line="170"/>
        <location filename="../../mainwindow.cpp" line="171"/>
        <location filename="../../mainwindow.cpp" line="172"/>
        <source>Full screen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="199"/>
        <source>Exit full screen(&amp;E)</source>
        <translation>退出全屏(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="200"/>
        <location filename="../../mainwindow.cpp" line="201"/>
        <location filename="../../mainwindow.cpp" line="202"/>
        <source>Exit full screen</source>
        <translation>退出全屏</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="325"/>
        <source>Open rabbit remote control file</source>
        <translation>打开玉兔远程控制文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="327"/>
        <source>Rabbit remote control Files (*.rrc);;All files(*.*)</source>
        <translation>玉兔远程控制文件(*.rrc);;所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="395"/>
        <source>Connecting to </source>
        <translation>正在连接 </translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="428"/>
        <source>Connected to </source>
        <translation>连接到 </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../main.cpp" line="56"/>
        <source>Rabbit Remote Control</source>
        <translation>玉兔远程控制</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="57"/>
        <source>Kang Lin studio</source>
        <translation>康林工作室</translation>
    </message>
</context>
</TS>
